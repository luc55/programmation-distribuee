/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entites;

import java.time.LocalDate;
import java.util.ArrayList;

/**
 *
 * @author N'KODODOBA Maloba Luc
 */
public class Test {
    public static void main(String[] args) {
        //Categories
        Categorie poissons = new Categorie(145, "Poissons", "VenteDePoisson");
        Categorie viandes = new Categorie(146, "Viandes", "VenteDeViande");

        //dates
        LocalDate date1 = LocalDate.of(2021, 2, 18);
        LocalDate date2 = LocalDate.of(2021, 5, 10);

        //produit
        Produit poisson1 = new Produit(147, "poisson1", 200, date1, poissons);
        Produit poisson2 = new Produit(148, "poisson2", 500, date2, poissons);
        Produit viande1 = new Produit(151, "viande1", 1500, date1, viandes);
        Produit viande2 = new Produit(152, "viande2", 1700, date2, viandes);

        //Produit achete
        ProduitAchete tilapia = new ProduitAchete(poisson1, 4, 0.5);
        ProduitAchete carpe = new ProduitAchete(poisson2, 2, 0.3);
        ProduitAchete poulet = new ProduitAchete(viande1, 3, 0.75);
        ProduitAchete ailes = new ProduitAchete(viande2, 5, 0.25);
        
        //produit
        ArrayList<ProduitAchete> array1 = new ArrayList<ProduitAchete>();
        array1.add(tilapia);
        array1.add(carpe);
        array1.add(poulet);
        array1.add(ailes);
        
        //Achat
        Achat achat1 = new Achat(106, 0.60, date1, array1);
        //Result
        System.out.println(achat1.toString());
        System.out.println(achat1.getPrixTotal()); 
    }
}
