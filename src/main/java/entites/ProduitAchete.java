package entites;

/**
 * @author N'KODODOBA Maloba Luc
 */
public class ProduitAchete {
    private Produit produit;
    private int quantite = 1;
    private double remise = 0.0;
    
    public ProduitAchete() {} //Default constructor

    public ProduitAchete(Produit produit, int quantite, double remise) {
        this.produit = produit;
        this.quantite = quantite;
        this.remise = remise;
    }
    
    
    public Produit getProduit() {
        return produit;
    }

    public void setProduit(Produit produit) {
        this.produit = produit;
    }

    public int getQuantite() {
        return quantite;
    }

    public void setQuantite(int quantite) {
        this.quantite = quantite;
    }

    public double getRemise() {
        return remise;
    }

    public void setRemise(double remise) {
        this.remise = remise;
    }
    
    
    public double getPrixTotal(){
        return this.produit.getPrixUnitaire() * quantite*(1 - remise);
    }

    @Override
    public String toString() {
        return "ProduitAchete{" + "produit=" + produit + ", quantite=" + quantite + ", remise=" + remise + '}';
    }
    
}
